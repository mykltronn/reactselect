import styled from 'styled-components';

export const App = styled.div`
    min-height: 100vh;
    background: steelblue;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

export const Container = styled.div`
    height: 400px;
    width: 700px;
    background: steelblue;
    box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const OptionValue = styled.p`
    width: 100%;
    padding: 1em .5em;
    margin: 0;
    /* &:hover {
        background: #ACACAC;
    } */
`;

export const ActionBar = styled.div`
    position: fixed;
    bottom: 0;
`;