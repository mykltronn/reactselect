import { useState } from 'react';
import * as Styled from './App.styled';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import theme from './Styles/theme'

import Select from './Select/Select';

import data from './data.json';

const GlobalStyle = createGlobalStyle`
  html {
    box-sizing: border-box;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
`;

function App() {
  const [city, setCity] = useState('');

  const filterOption = (value, option) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return option.name.toLowerCase().slice(0, inputLength) === inputValue
  }

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Styled.App>
        <div style={{ height: "80vh", width: "100%" }}></div>
        <Styled.Container>
          <Select.Select onSelection={c => setCity(c)} filterOption={filterOption} labelAccessor="name" >
            <Select.Label color="white" py={2} display="block">Cities</Select.Label>
            <Select.InputContainer >
              <Select.Input borderColor="grey" />
              <Select.OptionContainer boxShadow="depth5" border="none" borderColor="grey">
                {data.map(city => (
                  <Select.Option key={city.id} option={city}>
                    <Styled.OptionValue>{city.name}</Styled.OptionValue>
                  </Select.Option>
                ))}
                <Styled.ActionBar>
                  <p>Click me tho!</p>
                </Styled.ActionBar>
              </Select.OptionContainer>
            </Select.InputContainer>
          </Select.Select>
        </Styled.Container>
        <div style={{ height: "80vh", width: "100%" }}></div>
        <div style={{ height: "80vh", width: "100%" }}></div>
      </Styled.App>
    </ThemeProvider>
  );
}

export default App;
