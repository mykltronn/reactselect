import styled, { css } from 'styled-components';
import { space, layout, color, border, fontSize, boxShadow, colorStyle } from 'styled-system'
import { BOTTOM, LEFT, RIGHT, TOP } from './Select';

export const Wrapper = styled.div`
  ${space}
  ${layout}
`;

export const Label = styled.label`
  ${space}
  ${layout}
  ${color}
  ${border}
  ${fontSize}
`;

export const InputContainer = styled.div`
  position: relative;
`;

export const Input = styled.input`
  ${space}
  ${layout}
  ${color}
  ${border}
  ${fontSize}
`;
Input.propTypes = {
  ...space.propTypes,
  ...layout.propTypes,
  ...color.propTypes,
  ...border.propTypes,
  ...fontSize.propTypes
}
Input.defaultProps = {
  border: 'standard',
  borderRadius: 'standard',
  borderColor: 'grey',
  py: 2,
  px: 3,
  fontSize: 2,
  color: 'black',
}

export const OptionContainer = styled.ul`
  display: ${props => props.showContainer ? 'block' : 'none'};
  position: absolute;
  ${({ containerPosition }) => {
    switch (containerPosition) {
      case TOP: return css`
        bottom: 100%; /* renders ABOVE the wrapper */
      `;
      case RIGHT: return css`
        left: 100%; /* renders RIGHT of the wrapper */
      `;
      case BOTTOM: return css`
        top: 100%; /* renders BELOW of the wrapper */
      `;
      case LEFT: return css`
        right: 100%; /* renders LEFT of the wrapper */
      `;
      default: return css`
        top: 100%; /* renders BELOW the wrapper */
      `;
    }
  }}
  min-width: 100%;
  overflow-y: scroll;
  list-style: none;
  ${layout}
  ${color}
  ${space}
  ${border}
  ${fontSize}
  ${boxShadow}
`;
OptionContainer.propTypes = {
  ...space.propTypes,
  ...color.propTypes,
  ...layout.propTypes,
  ...border.propTypes,
  ...fontSize.propTypes,
  ...boxShadow.propTypes,
}
OptionContainer.defaultProps = {
  border: 'standard',
  borderRadius: 'standard',
  borderColor: 'grey',
  m: 0,
  p: 0,
  bg: 'white',
  color: 'black',
  fontSize: 2,
  maxHeight: 5,
  boxShadow: undefined
}

export const Option = styled.li`
  cursor: pointer;
  &:focus {
    background: "#ACACAC"
  }
`;
Option.propTypes = {
  ...color.propTypes,
  ...space.propTypes,
}
Option.defaultProps = {
  px: 3,
  py: 1
}
