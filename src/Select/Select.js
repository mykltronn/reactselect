import React, { createContext, createRef, useRef, useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import * as Styled from './Select.styled';
import useIntersectionObserver from '../Hooks/useIntersectionObserver';

const SelectContext = createContext();

function Select({ children, onSelection, labelAccessor, filterOption, ...props }) {
    const containerRef = createRef();
    const inputRef = createRef();
    const optionRefs = useRef([]);
    // const [optionRefs, setOptionRefs] = useState([]);
    const [showContainer, setShowContainer] = useState(false);
    const [selectedOption, setSelectedOption] = useState('');
    const [inputValue, setInputValue] = useState('');

    useEffect(() => {
        function _toggleOpenOnFocus(e) {
            e.stopPropagation()
            const targetIsInput = e.target.closest('input') === inputRef.current;
            if (targetIsInput) setShowContainer(true)
            else setShowContainer(false)
        }
        document.addEventListener("click", _toggleOpenOnFocus)
        return () => document.removeEventListener('click', _toggleOpenOnFocus)
    }, [inputRef, containerRef])

    const handleOptionSelected = option => {
        setInputValue(option[labelAccessor])
        setSelectedOption(option)
        onSelection(option)
        setShowContainer(false);
    }

    const handleSetInputValue = value => {
        setInputValue(value)
    }

    /* Handling various keypress events */

    const _setFocusToContainer = () => {
        // if there's a selectedOption, set focus to it
        if (selectedOption) {
            const so = selectedOption
            const r = optionRefs
            debugger;
            // set focus to selected option
        } else {
            // set focus to first option in list.
            if (optionRefs.current[0]) {
                console.log('focusing: ', optionRefs.current[0])
                optionRefs.current[0].focus()
            }
        }
    }

    const _handleInputArrowDown = () => {
        // Make sure container is open first!
        setShowContainer(true)
        // set focus to selected element
        _setFocusToContainer();
    }

    const _handleInputArrowUp = () => {
        setShowContainer(false);
    }

    const handleKeyPressedInInput = ({ key }) => {
        if (key === "ArrowDown") _handleInputArrowDown();
        if (key === "ArrowUp") _handleInputArrowUp();
    }

    useEffect(() => {
        console.log('optionRefs ', optionRefs)
    }, [optionRefs])

    return (
        <SelectContext.Provider value={{
            showContainer,
            setShowContainer,
            containerRef,
            inputRef,
            handleOptionSelected,
            selectedOption,
            inputValue,
            handleSetInputValue,
            filterOption,
            handleKeyPressedInInput,
            optionRefs
        }}>
            <Styled.Wrapper {...props}>
                {children}
            </Styled.Wrapper>
        </SelectContext.Provider>

    );
};

Select.propTypes = {
    onSelection: PropTypes.func.isRequired,
}

function Label({ children, ...props }) {
    return (
        <Styled.Label {...props}>{children}</Styled.Label>
    )
}

function InputContainer({ children, ...props }) {
    return (
        <Styled.InputContainer {...props}>{children}</Styled.InputContainer>
    )
}

function Input({ children, ...props }) {
    const { inputRef, inputValue, handleSetInputValue, handleKeyPressedInInput } = useContext(SelectContext)

    return (
        <Styled.Input
            {...props}
            value={inputValue}
            onChange={e => handleSetInputValue(e.target.value)}
            onKeyDown={handleKeyPressedInInput}
            ref={inputRef}
        />
    )
}

Input.propTypes = {
    labelAccessor: PropTypes.string
}

Input.defaultProps = {
    labelAccessor: 'label'
}

export const TOP = 'TOP'
export const RIGHT = 'RIGHT'
export const BOTTOM = 'BOTTOM'
export const LEFT = 'LEFT'

function OptionContainer({ children, scrollArea, ...props }) {
    const { containerRef, showContainer } = useContext(SelectContext)
    const [containerPosition, setContainerPosition] = useState(BOTTOM)
    const intersections = useIntersectionObserver(containerRef, scrollArea)

    // listen to intersections for changes, set position accordingly
    useEffect(() => {
        if (intersections.topHit) setContainerPosition(BOTTOM)
        if (intersections.rightHit) setContainerPosition(LEFT)
        if (intersections.bottomHit) setContainerPosition(TOP)
        if (intersections.leftHit) setContainerPosition(RIGHT)
    }, [intersections])

    return (
        <Styled.OptionContainer {...props} showContainer={showContainer} containerPosition={containerPosition} ref={containerRef}>
            {children}
        </Styled.OptionContainer>
    )
}

function Option({ option, children, ...props }) {
    const { handleOptionSelected, inputValue, filterOption, optionRefs } = useContext(SelectContext)


    // useEffect(() => {
    //     console.log('optionRef', optionRef);
    //     console.log('optionRefs', optionRefs)
    //     if (optionRef && !optionRefs?.current.includes(optionRef)) {
    //         console.log('it is not in there, so add it!')
    //         optionRefs.push(optionRef)
    //     }
    // }, [optionRef, optionRefs])

    const _setRef = ref => {
        if (!optionRefs.current.find(r => r === ref?.current)) {
            optionRefs.current.push(ref)
        }
    }

    if (filterOption(inputValue, option)) {
        return (
            <Styled.Option
                tabIndex="-1"
                {...props}
                onMouseUp={() => handleOptionSelected(option)}
                ref={_setRef}
            >
                {children}
            </Styled.Option>
        )
    }
    return null
}


const modules = {
    Select,
    Label,
    InputContainer,
    Input,
    OptionContainer,
    Option,
}
export default modules;
